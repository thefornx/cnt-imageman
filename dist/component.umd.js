(function (global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('axios')) :
  typeof define === 'function' && define.amd ? define(['exports', 'axios'], factory) :
  (global = global || self, factory(global.CntImageman = {}, global.axios));
}(this, (function (exports, axios) { 'use strict';

  axios = axios && Object.prototype.hasOwnProperty.call(axios, 'default') ? axios['default'] : axios;

  //

  var script = {
    props: [ 'endpoint' ],
    data: function data () {
      return {
        outlet: '',
        album: '',
        name: '',
        image: '',
        approveFiles: ['data:image/jpeg','data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet','data:application/pdf'],
        approve: false,
        uploadedImg: false
      }
    },
    methods: {
      uploadImg: function uploadImg () {
        var queryParam = "?outlet=" + (this.outlet);
        queryParam = this.album ? (queryParam + "&album=" + (this.album)) : queryParam;
        queryParam = this.name ? (queryParam + "&name=" + (this.name)) : queryParam;

        if (this.image === '') { return alert('Image needed'); }
        var img = this.image;
        var _this = this;

        axios.get(("" + (this.endpoint) + queryParam))
        .then (function (response) {
          console.log(response.data.uploadUrl);

          var binary = atob(img.split(',')[1]);
          var array = [];
          for (var i = 0; i < binary.length; i++) {
            array.push(binary.charCodeAt(i));
          }
          var blobData = new Blob([new Uint8Array(array)], {type: 'image/jpg'});
          fetch(response.data.uploadUrl, {
            method: 'PUT',
            body: blobData
          }).then (function () {
            _this.uploadedImg = true;
          });
        });
      },
      onChange: function onChange (e) {
        var files = e.target.files || e.dataTransfer.files;
        if (!files.length) { return }
        this.checkImage(files[0]);
      },
      checkImage: function checkImage (file) {
        var this$1 = this;

        var reader = new FileReader();
        reader.onload = function (e) {
          this$1.approveFiles.forEach(function (type) {
            if (!e.target.result.includes(type)) {
              this$1.approve = true;
            }
          });
          if(!this$1.approve){
            return alert('Төрөл таарахгүй')
          }
          this$1.image = e.target.result;
        };
        reader.readAsDataURL(file);
      },
      nextBtn: function nextBtn () {
        this.uploadedImg = false;
        this.outlet = '';
        this.album = '';
        this.name = '';
        this.image = '';
        this.approve = false;
      }
    }
  };

  function normalizeComponent(template, style, script, scopeId, isFunctionalTemplate, moduleIdentifier /* server only */, shadowMode, createInjector, createInjectorSSR, createInjectorShadow) {
      if (typeof shadowMode !== 'boolean') {
          createInjectorSSR = createInjector;
          createInjector = shadowMode;
          shadowMode = false;
      }
      // Vue.extend constructor export interop.
      var options = typeof script === 'function' ? script.options : script;
      // render functions
      if (template && template.render) {
          options.render = template.render;
          options.staticRenderFns = template.staticRenderFns;
          options._compiled = true;
          // functional template
          if (isFunctionalTemplate) {
              options.functional = true;
          }
      }
      // scopedId
      if (scopeId) {
          options._scopeId = scopeId;
      }
      var hook;
      if (moduleIdentifier) {
          // server build
          hook = function (context) {
              // 2.3 injection
              context =
                  context || // cached call
                      (this.$vnode && this.$vnode.ssrContext) || // stateful
                      (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext); // functional
              // 2.2 with runInNewContext: true
              if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
                  context = __VUE_SSR_CONTEXT__;
              }
              // inject component styles
              if (style) {
                  style.call(this, createInjectorSSR(context));
              }
              // register component module identifier for async chunk inference
              if (context && context._registeredComponents) {
                  context._registeredComponents.add(moduleIdentifier);
              }
          };
          // used by ssr in case component is cached and beforeCreate
          // never gets called
          options._ssrRegister = hook;
      }
      else if (style) {
          hook = shadowMode
              ? function (context) {
                  style.call(this, createInjectorShadow(context, this.$root.$options.shadowRoot));
              }
              : function (context) {
                  style.call(this, createInjector(context));
              };
      }
      if (hook) {
          if (options.functional) {
              // register for functional component in vue file
              var originalRender = options.render;
              options.render = function renderWithStyleInjection(h, context) {
                  hook.call(context);
                  return originalRender(h, context);
              };
          }
          else {
              // inject component registration as beforeCreate hook
              var existing = options.beforeCreate;
              options.beforeCreate = existing ? [].concat(existing, hook) : [hook];
          }
      }
      return script;
  }

  var isOldIE = typeof navigator !== 'undefined' &&
      /msie [6-9]\\b/.test(navigator.userAgent.toLowerCase());
  function createInjector(context) {
      return function (id, style) { return addStyle(id, style); };
  }
  var HEAD;
  var styles = {};
  function addStyle(id, css) {
      var group = isOldIE ? css.media || 'default' : id;
      var style = styles[group] || (styles[group] = { ids: new Set(), styles: [] });
      if (!style.ids.has(id)) {
          style.ids.add(id);
          var code = css.source;
          if (css.map) {
              // https://developer.chrome.com/devtools/docs/javascript-debugging
              // this makes source maps inside style tags work properly in Chrome
              code += '\n/*# sourceURL=' + css.map.sources[0] + ' */';
              // http://stackoverflow.com/a/26603875
              code +=
                  '\n/*# sourceMappingURL=data:application/json;base64,' +
                      btoa(unescape(encodeURIComponent(JSON.stringify(css.map)))) +
                      ' */';
          }
          if (!style.element) {
              style.element = document.createElement('style');
              style.element.type = 'text/css';
              if (css.media)
                  { style.element.setAttribute('media', css.media); }
              if (HEAD === undefined) {
                  HEAD = document.head || document.getElementsByTagName('head')[0];
              }
              HEAD.appendChild(style.element);
          }
          if ('styleSheet' in style.element) {
              style.styles.push(code);
              style.element.styleSheet.cssText = style.styles
                  .filter(Boolean)
                  .join('\n');
          }
          else {
              var index = style.ids.size - 1;
              var textNode = document.createTextNode(code);
              var nodes = style.element.childNodes;
              if (nodes[index])
                  { style.element.removeChild(nodes[index]); }
              if (nodes.length)
                  { style.element.insertBefore(textNode, nodes[index]); }
              else
                  { style.element.appendChild(textNode); }
          }
      }
  }

  /* script */
  var __vue_script__ = script;

  /* template */
  var __vue_render__ = function() {
    var _vm = this;
    var _h = _vm.$createElement;
    var _c = _vm._self._c || _h;
    return _c("div", { staticClass: "uploader-body" }, [
      !_vm.uploadedImg
        ? _c("div", [
            _c("input", {
              staticClass: "image-upload",
              attrs: { type: "file", "aria-label": "Image uploader" },
              on: { change: _vm.onChange }
            }),
            _vm._v(" "),
            _c("div", { staticClass: "dir-container" }, [
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.outlet,
                    expression: "outlet"
                  }
                ],
                staticClass: "file-dir",
                attrs: { type: "text", placeholder: "Outlet" },
                domProps: { value: _vm.outlet },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.outlet = $event.target.value;
                  }
                }
              }),
              _vm._v(" "),
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.album,
                    expression: "album"
                  }
                ],
                staticClass: "file-dir",
                attrs: { type: "text", placeholder: "Album" },
                domProps: { value: _vm.album },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.album = $event.target.value;
                  }
                }
              }),
              _vm._v(" "),
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.name,
                    expression: "name"
                  }
                ],
                staticClass: "file-dir",
                attrs: { type: "text", placeholder: "Name" },
                domProps: { value: _vm.name },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.name = $event.target.value;
                  }
                }
              }),
              _vm._v(" "),
              _c("button", { on: { click: _vm.uploadImg } }, [_vm._v("Upload")])
            ])
          ])
        : _vm._e(),
      _vm._v(" "),
      _vm.uploadedImg
        ? _c("div", [
            _c("div", { staticClass: "text" }, [_vm._v("Successfully uploaded")]),
            _vm._v(" "),
            _c("div", { staticClass: "next-btn", on: { click: _vm.nextBtn } }, [
              _c("button", [_vm._v("Upload next »")])
            ])
          ])
        : _vm._e()
    ])
  };
  var __vue_staticRenderFns__ = [];
  __vue_render__._withStripped = true;

    /* style */
    var __vue_inject_styles__ = function (inject) {
      if (!inject) { return }
      inject("data-v-7af62520_0", { source: "\n*[data-v-7af62520] {\n  box-sizing: border-box;\n}\n.uploader-body[data-v-7af62520] {\n  position: relative;\n  box-shadow: 0 0 10px #e0e0e0;\n  max-width: 500px;\n  min-width: 100px;\n  padding: 25px;\n}\n.image-upload[data-v-7af62520] {\n  background-color: #f0f0f0;\n  width: 100%;\n  border-radius: 5px;\n}\n.image-upload[data-v-7af62520]::-webkit-file-upload-button {\n  border: none;\n  padding: 10px 20px;\n  background-color: #505050;\n  color: #ffffff;\n  cursor: pointer;\n  border-radius: 5px 0 0 5px;\n}\n.file-dir[data-v-7af62520] {\n  width: 100%;\n  border: none;\n  background-color: #f0f0f0;\n  border-radius: 5px;\n  padding: 10px 20px;\n  margin-right: 5px;\n  outline: none;\n}\n.uploader-body .dir-container[data-v-7af62520] {\n  display: flex;\n  border-top: 1px solid #eeeeee;\n  padding-top: 10px;\n  margin-top: 10px;\n}\nbutton[data-v-7af62520] {\n  padding: 10px 20px;\n  border: none;\n  border-radius: 5px;\n  background-color: #505050;\n  color: #ffffff;\n  cursor: pointer;\n}\n.text[data-v-7af62520] {\n  font-size: x-large;\n  color: #505050;\n  text-align: center;\n}\n.next-btn[data-v-7af62520] {\n  display: flex;\n  padding-top: 10px;\n  margin-top: 10px;\n  border-top: 1px solid #eeeeee;\n  justify-content: flex-end;\n}\n", map: {"version":3,"sources":["/Users/macbppkpro/Documents/nodepackage/cnt-imgman/src/component.vue"],"names":[],"mappings":";AA+FA;EACA,sBAAA;AACA;AACA;EACA,kBAAA;EACA,4BAAA;EACA,gBAAA;EACA,gBAAA;EACA,aAAA;AACA;AACA;EACA,yBAAA;EACA,WAAA;EACA,kBAAA;AACA;AACA;EACA,YAAA;EACA,kBAAA;EACA,yBAAA;EACA,cAAA;EACA,eAAA;EACA,0BAAA;AACA;AACA;EACA,WAAA;EACA,YAAA;EACA,yBAAA;EACA,kBAAA;EACA,kBAAA;EACA,iBAAA;EACA,aAAA;AACA;AAEA;EACA,aAAA;EACA,6BAAA;EACA,iBAAA;EACA,gBAAA;AACA;AACA;EACA,kBAAA;EACA,YAAA;EACA,kBAAA;EACA,yBAAA;EACA,cAAA;EACA,eAAA;AACA;AACA;EACA,kBAAA;EACA,cAAA;EACA,kBAAA;AACA;AACA;EACA,aAAA;EACA,iBAAA;EACA,gBAAA;EACA,6BAAA;EACA,yBAAA;AACA","file":"component.vue","sourcesContent":["<template>\n  <div class=\"uploader-body\">\n    <div v-if=\"!uploadedImg\">\n      <input type=\"file\" class=\"image-upload\" aria-label=\"Image uploader\" @change=\"onChange\">\n      <div class=\"dir-container\">\n        <input type=\"text\" class=\"file-dir\" placeholder=\"Outlet\" v-model=\"outlet\">\n        <input type=\"text\" class=\"file-dir\" placeholder=\"Album\" v-model=\"album\">\n        <input type=\"text\" class=\"file-dir\" placeholder=\"Name\" v-model=\"name\">\n        <button @click=\"uploadImg\">Upload</button>\n      </div>\n    </div>\n    <div v-if=\"uploadedImg\">\n      <div class=\"text\">Successfully uploaded</div>\n      <div class=\"next-btn\" @click=\"nextBtn\"><button>Upload next »</button></div>\n    </div>\n  </div>\n</template>\n\n<script>\nimport axios from 'axios'\n\nexport default {\n  props: [ 'endpoint' ],\n  data () {\n    return {\n      outlet: '',\n      album: '',\n      name: '',\n      image: '',\n      approveFiles: ['data:image/jpeg','data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet','data:application/pdf'],\n      approve: false,\n      uploadedImg: false\n    }\n  },\n  methods: {\n    uploadImg () {\n      let queryParam = `?outlet=${this.outlet}`\n      queryParam = this.album ? `${queryParam}&album=${this.album}` : queryParam\n      queryParam = this.name ? `${queryParam}&name=${this.name}` : queryParam\n\n      if (this.image === '') return alert('Image needed');\n      let img = this.image\n      let _this = this\n\n      axios.get(`${this.endpoint}${queryParam}`)\n      .then (function (response) {\n        console.log(response.data.uploadUrl);\n\n        let binary = atob(img.split(',')[1])\n        let array = []\n        for (var i = 0; i < binary.length; i++) {\n          array.push(binary.charCodeAt(i))\n        }\n        let blobData = new Blob([new Uint8Array(array)], {type: 'image/jpg'})\n        fetch(response.data.uploadUrl, {\n          method: 'PUT',\n          body: blobData\n        }).then (function () {\n          _this.uploadedImg = true\n        })\n      })\n    },\n    onChange (e) {\n      let files = e.target.files || e.dataTransfer.files\n      if (!files.length) return\n      this.checkImage(files[0])\n    },\n    checkImage (file) {\n      let reader = new FileReader()\n      reader.onload = (e) => {\n        this.approveFiles.forEach(type => {\n          if (!e.target.result.includes(type)) {\n            this.approve = true\n          }\n        });\n        if(!this.approve){\n          return alert('Төрөл таарахгүй')\n        }\n        this.image = e.target.result\n      }\n      reader.readAsDataURL(file)\n    },\n    nextBtn () {\n      this.uploadedImg = false\n      this.outlet = ''\n      this.album = ''\n      this.name = ''\n      this.image = ''\n      this.approve = false\n    }\n  }\n}\n</script>\n\n<style scoped>\n  * {\n    box-sizing: border-box;\n  }\n  .uploader-body {\n    position: relative;\n    box-shadow: 0 0 10px #e0e0e0;\n    max-width: 500px;\n    min-width: 100px;\n    padding: 25px;\n  }\n  .image-upload {\n    background-color: #f0f0f0;\n    width: 100%;\n    border-radius: 5px;\n  }\n  .image-upload::-webkit-file-upload-button {\n    border: none;\n    padding: 10px 20px;\n    background-color: #505050;\n    color: #ffffff;\n    cursor: pointer;\n    border-radius: 5px 0 0 5px;\n  }\n  .file-dir {\n    width: 100%;\n    border: none;\n    background-color: #f0f0f0;\n    border-radius: 5px;\n    padding: 10px 20px;\n    margin-right: 5px;\n    outline: none;\n  }\n\n  .uploader-body .dir-container {\n    display: flex;\n    border-top: 1px solid #eeeeee;\n    padding-top: 10px;\n    margin-top: 10px;\n  }\n  button {\n    padding: 10px 20px;\n    border: none;\n    border-radius: 5px;\n    background-color: #505050;\n    color: #ffffff;\n    cursor: pointer;\n  }\n  .text {\n    font-size: x-large;\n    color: #505050;\n    text-align: center;\n  }\n  .next-btn {\n    display: flex;\n    padding-top: 10px;\n    margin-top: 10px;\n    border-top: 1px solid #eeeeee;\n    justify-content: flex-end;\n  }\n</style>"]}, media: undefined });

    };
    /* scoped */
    var __vue_scope_id__ = "data-v-7af62520";
    /* module identifier */
    var __vue_module_identifier__ = undefined;
    /* functional template */
    var __vue_is_functional_template__ = false;
    /* style inject SSR */
    
    /* style inject shadow dom */
    

    
    var __vue_component__ = /*#__PURE__*/normalizeComponent(
      { render: __vue_render__, staticRenderFns: __vue_staticRenderFns__ },
      __vue_inject_styles__,
      __vue_script__,
      __vue_scope_id__,
      __vue_is_functional_template__,
      __vue_module_identifier__,
      false,
      createInjector,
      undefined,
      undefined
    );

  // Import vue component

  // Declare install function executed by Vue.use()
  function install(Vue) {
  	if (install.installed) { return; }
  	install.installed = true;
  	Vue.component('uploader', __vue_component__);
  }

  // Create module definition for Vue.use()
  var plugin = {
  	install: install,
  };

  // Auto-install when vue is found (eg. in browser via <script> tag)
  var GlobalVue = null;
  if (typeof window !== 'undefined') {
  	GlobalVue = window.Vue;
  } else if (typeof global !== 'undefined') {
  	GlobalVue = global.Vue;
  }
  if (GlobalVue) {
  	GlobalVue.use(plugin);
  }

  exports.default = __vue_component__;
  exports.install = install;

  Object.defineProperty(exports, '__esModule', { value: true });

})));
